/**
 * Entity
 * ----------------------------------------------------------------------------
 * Base entity functionality for Parsec Windows and UIElement
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */

#include <cstdio>
#include <parsec/Point.h>
#include <parsec/Entity.h>
#include <parsec/UIElement.h>

namespace parsec
{

	void Entity::render(Bounds bounds)
	{
		// Render all child UI elements
		for(std::list<UIElement *>::iterator it = _children.begin(); it != _children.end(); ++it)
		{
			UIElement *element = *it;
			
			// Translate bounds of child to make it absolute to the window
			Bounds childBounds(Point(bounds.position.x + element->position().x, 
									 bounds.position.y + element->position().y), 
							   element->size());
			
			// Render the child
			element->render(childBounds);
		}
	}
	
	void Entity::addChild(UIElement *element)
	{
		element->_parent = this;
		_children.push_back(element);
	}
	
	void Entity::removeChild(UIElement *element)
	{
		element->_parent = NULL;
		_children.remove(element);
	}

	bool Entity::handleMouseMove(MouseEventArgs e)
	{
		// Has the event already been handled?
		if(e.handled)
		{
			// Mouse event was already handled. If the mouse had "entered" earlier, 
			// it has now left.
			if(_entered)
			{
				// Mouse has left.
				handleMouseEnter(e);
			}
			
			// Can't do anything else since the event has already been handled
			return true;
		}
	
		// Let all child UI elements handle mouse move events
		for(std::list<UIElement *>::reverse_iterator it = _children.rbegin(); it != _children.rend(); ++it)
		{
			UIElement *child = *it;
			
			// Make positions relative to child
			MouseEventArgs ce = e;
			ce.x = e.x - child->position().x;
			ce.y = e.y - child->position().y;
			
			// Have child handle the mouse move data
			e.handled = child->handleMouseMove(ce);
		}
		
		if(!e.handled)
		{
			// Children haven't handled the event, can we handle it?
			if(this->hitTest(e.x, e.y))
			{
				// Let people know
				mouse_move(this, e);
				
				// Have we been entered before? Perhaps mouse just 
				// got in.
				if(!_entered)
				{
					handleMouseEnter(e);
				}
			
				// Handled
				return true;
			}
			else
			{
				// We couldn't handle it either. Maybe mouse has left.
				if(_entered)
				{
					handleMouseLeave(e);
				}
			}
		}
		
		// All done
		return e.handled;
	}
	
	bool Entity::handleMouseScroll(MouseEventArgs e)
	{
		// Let all child UI elements handle mouse scroll events
		for(std::list<UIElement *>::reverse_iterator it = _children.rbegin(); it != _children.rend(); ++it)
		{
			UIElement *child = *it;
			
			// Make positions relative to child
			MouseEventArgs ce = e;
			ce.x = e.x - child->position().x;
			ce.y = e.y - child->position().y;
			
			if(child->handleMouseScroll(ce))
			{
				return true;
			}
		}
		
		// Children haven't handled the event, can we handle it?
		if(this->hitTest(e.x, e.y))
		{
			// Let people know
			mouse_scroll(this, e);
			
			// All done
			return true;
		}
		
		return false;
	}
	
	bool Entity::handleMouseButton(MouseEventArgs e)
	{
		// Let all child UI elements handle mouse button events
		for(std::list<UIElement *>::reverse_iterator it = _children.rbegin(); it != _children.rend(); ++it)
		{
			UIElement *child = *it;
			
			// Make positions relative to child
			MouseEventArgs ce = e;
			ce.x = e.x - child->position().x;
			ce.y = e.y - child->position().y;
			
			if(child->handleMouseButton(ce))
			{
				_mouse_down = false;
				return true;
			}
		}
		
		// Children haven't handled the event, can we handle it?
		if(this->hitTest(e.x, e.y))
		{
			// Let people know
			if(e.action == 0)
			{
				_mouse_down = true;
				mouse_down(this, e);
			}
			else
			{
				_mouse_down = false;
				mouse_up(this, e);
			}
			
			// All done
			return true;
		}
		
		return false;
	}
	
	void Entity::handleMouseEnter(MouseEventArgs e)
	{
		_entered = true;
		mouse_enter(this, e);
	}
	
	void Entity::handleMouseLeave(MouseEventArgs e)
	{
		_entered = false;
		mouse_leave(this, e);
	}
	
	bool Entity::handleKey(KeyEventArgs e)
	{
		// Let all child UI elements handle keyboard events
		for(std::list<UIElement *>::reverse_iterator it = _children.rbegin(); it != _children.rend(); ++it)
		{
			UIElement *child = *it;
			if(child->handleKey(e))
			{
				return true;
			}
		}
		
		return false;
	}
	
	bool Entity::hitTest(double x, double y)
	{
		if(x >= 0 && x <= _size.width && 
		   y >= 0 && y <= _size.height)
		{
			return true;
		}
		return false;
	}

	int Entity::_counter = 0;
}
