/**
 * Parsec Library Entry Point
 * ----------------------------------------------------------------------------
 * Main library / API entry point.
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#include <cstdio>
#include <stdexcept>
#include <SFML/Window.hpp>
#include <parsec/Parsec.h>

namespace parsec
{
	
	void Parsec::initialize()
	{
		
	}

	sf::Time Parsec::time()
	{
		return _systemClock.getElapsedTime();
	}
	
	void Parsec::think()
	{
		for(std::set<Window *>::iterator it = _registered_windows.begin(); 
			it != _registered_windows.end();
			++it)
		{
			Window *window = *it;
			if(window)
			{
				window->think();
			}
		}
	}
	
	void Parsec::render()
	{
		for(std::set<Window *>::iterator it = _registered_windows.begin(); 
			it != _registered_windows.end();
			++it)
		{
			Window *window = *it;
			if(window)
			{
				window->render();
			}
		}
		
		// Calculate FPS
		_fps_counter++;
		float now = time().asSeconds();
		if(now - _fps_last_time >= 1.0)
		{
			_fps = _fps_counter;
			_fps_counter = 0;
			_fps_last_time = now;
			printf("FPS = %d\n", fps());
		}
	}
	
	void Parsec::shutdown()
	{
		
	}

	void Parsec::registerWindow(Window *window)
	{
		// ALWAYS REMEMBER
		_registered_windows.insert(window);
	}
	
	void Parsec::deregisterWindow(Window *window)
	{
		// BYE BYE
		_registered_windows.erase(window);
	}
	
	sf::Clock	Parsec::_systemClock;
	
	std::set<Window *> 	Parsec::_registered_windows;
	
	int 		Parsec::_fps = 0;
	int 		Parsec::_fps_counter = 0;
	float 		Parsec::_fps_last_time = 0.0;
	
	
}
