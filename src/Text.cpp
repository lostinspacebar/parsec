/**
 * Text
 * ----------------------------------------------------------------------------
 * Simple text layer. Draws text on screen. Doesn't care about events.
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#include <cstdio>
#include <parsec/Text.h>

namespace parsec
{

	Text::Text() : UIElement()
	{		
		
	}

	void Text::render()
	{
		
	}
}
