/**
 * Window
 * ----------------------------------------------------------------------------
 * OS specific window
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#include <cstdio>
#include <stdexcept>
#include <parsec/Parsec.h>
#include <parsec/Window.h>
#include <SFML/OpenGL.hpp>

namespace parsec
{
	Window::Window(std::string title, Size size) : Entity(title)
	{
		_title 	= title;
		_closed = false;
		_size = size;
		
		// Create an SFML window
		_window.create(sf::VideoMode(size.width, size.height), title);
		_window.setVerticalSyncEnabled(true);
		
		// Register the window with the Parsec library
		Parsec::registerWindow(this);
	}
	
	void Window::render()
	{
		// Clear the window
		_window.clear(sf::Color::Black);
		
		// Perform base operations (this renders all children)
		Entity::render(Bounds(0, 0, _size.width, _size.height));
		
		// Swap buffers and display the window contents
		_window.display();
	}
	
	void Window::think()
	{
		sf::Event event;
		while(_window.pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
			{
				_window.close();
			}
		}
	}

}
