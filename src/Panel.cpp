/**
 * UI Panel
 * ----------------------------------------------------------------------------
 * Simple UI panel. Usually used as a container for other UIElements.
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#include <cstdio>
#include <parsec/Panel.h>
#include <SFML/OpenGL.hpp>

namespace parsec
{
	void Panel::render(Bounds bounds)
	{	
		_shape.setPosition(sf::Vector2f(bounds.position.x, bounds.position.y));
		_shape.setSize(sf::Vector2f(bounds.size.width, bounds.size.height));
		
		printf("%s: {%d, %d, %d, %d}\n", _name.c_str(), bounds.position.x, bounds.position.y, bounds.size.width, bounds.size.height);
		
		// Base render stuff (this also takes care of rendering children)
		UIElement::render(bounds);
	}
}
