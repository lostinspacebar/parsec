CC=g++
CFLAGS=-c -fpic -I/usr/local/include -I./include -I./lib
LFLAGS=`pkg-config --libs sfml-all`

all: lib

parsec: src/Parsec.cpp
	$(CC) $(CFLAGS) src/Parsec.cpp

window: src/Window.cpp
	$(CC) $(CFLAGS) src/Window.cpp
	
panel: src/Panel.cpp
	$(CC) $(CFLAGS) src/Panel.cpp
	
entity: src/Entity.cpp
	$(CC) $(CFLAGS) src/Entity.cpp
	
text: src/Text.cpp
	$(CC) $(CFLAGS) src/Text.cpp

lib: parsec window panel entity text
	$(CC) -shared -o libparsec.so Panel.o Window.o Parsec.o Entity.o Text.o $(LFLAGS)
	
clean:
	rm -rf *.o *.so
