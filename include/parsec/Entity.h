/**
 * Entity
 * ----------------------------------------------------------------------------
 * Base entity functionality for Parsec Windows and UIElement
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#ifndef PARSEC_ENTITY_H_
#define PARSEC_ENTITY_H_

#include <sstream>
#include <list>
#include <Signals/include/Signal.h>
#include <parsec/Point.h>
#include <parsec/Size.h>
#include <parsec/Bounds.h>
#include <parsec/Color.h>
#include <parsec/MouseEventArgs.h>

using namespace Gallant;

namespace parsec
{
	class UIElement;

	class Entity
	{
	public:
		Signal2<Entity *, MouseEventArgs> mouse_up;
		Signal2<Entity *, MouseEventArgs> mouse_down;
		Signal2<Entity *, MouseEventArgs> mouse_move;
		Signal2<Entity *, MouseEventArgs> mouse_enter;
		Signal2<Entity *, MouseEventArgs> mouse_leave;
		Signal2<Entity *, MouseEventArgs> mouse_scroll;
		
		Signal2<Entity *, KeyEventArgs>	key_up;
		Signal2<Entity *, KeyEventArgs>	key_down;
		
		Signal1<Entity *> focus;
		Signal1<Entity *> blur;
		
		Entity()
		{
			_counter++;
			_name = "Entity";
			
			std::stringstream counter_string;
			counter_string << _counter;
			_name.append(counter_string.str());
		}
		
		Entity(std::string name)
		{
			_counter++;
			_name = name;
		}
		
		virtual std::string name() const
		{
			return _name;
		}
		
		virtual void setName(std::string name)
		{
			_name = name;
		}
	
		virtual Point position() const
		{
			return _position;
		}
		
		virtual void setPosition(int x, int y)
		{
			_position.x = x;
			_position.y = y;
		}
		
		virtual void setPosition(Point p)
		{
			setPosition(p.x, p.y);
		}
		
		virtual Size size() const
		{
			return _size;
		}
		
		virtual void setSize(int width, int height)
		{
			_size.width = width;
			_size.height = height;
		}
		
		virtual void setSize(Size s)
		{
			setSize(s.width, s.height);
		}
		
		virtual Color backgroundColor() const
		{
			return _background_color;
		}
		
		virtual void setBackgroundColor(float r, float g, float b, float a = 1.0f)
		{
			setBackgroundColor(Color(r, g, b, a));
		}
		
		virtual void setBackgroundColor(Color c)
		{
			_background_color = c;
		}
		
		virtual Color foregroundColor() const
		{
			return _foreground_color;
		}
		
		virtual void setForegroundColor(float r, float g, float b, float a = 1.0f)
		{
			setForegroundColor(Color(r, g, b, a));
		}
		
		virtual void setForegroundColor(Color c)
		{
			_foreground_color = c;
		}
		
		/**
		 * Get all child elements of this Entity as an iterator.
		 *
		 */
		std::list<UIElement *> *children()
		{
			return &_children;
		}
		
		/**
		 * Gets the parent Entity. If NULL, this entity has no parents. 
		 * Window instances never have a parent.
		 *
		 */
		Entity *parent()
		{
			return _parent;
		}
		
		/**
		 * Gets whether the UI element has focus.
		 *
		 */
		bool focused() const
		{
			return _focused;
		}
		
		/**
		 * Render the entity to screen given it's screen translated bounds. This position
		 * and size data is relative to the window (i.e. absolute size and position) as 
		 * opposed to the element's parent.
		 *
		 */
		virtual void render(Bounds bounds);
		
		/**
		 * Entity think function. Allows the entity to do any logic every frame
		 *
		 */
		virtual void think() = 0;
		
		/**
		 * Add a new UIElement as a child to this UIElement.
		 *
		 * @param element	UIElement to be added
		 *
		 */
		virtual void addChild(UIElement *element);
		
		/**
		 * Removes a UIElement from this element's children.
		 *
		 * @param element	UIElement to be removed
		 *
		 */
		virtual void removeChild(UIElement *element);
		
	protected:
		
		/**
		 * Handle mouse move events from the window. The entity is trusted to 
		 * only handle mouse events if they are relevant to itself.
		 *
		 * If the entity has handled the event, it should return true. The 
		 * parent entity will then stop looking through it's children to 
		 * see who will handle the event.
		 * 
		 * @param e		Mouse event data
		 * @return		true if the Entity has handled the event
		 *
		 */
		virtual bool handleMouseMove(MouseEventArgs e);
		
		/**
		 * Handle mouse scroll events from the window. The entity is trusted 
		 * to only handle mouse events if they are relevant to itself.
		 *
		 * If the entity has handled the event, it should return true. The 
		 * parent entity will then stop looking through it's children to 
		 * see who will handle the event.
		 * 
		 * @param e		Mouse event data
		 * @return		true if the Entity has handled the event
		 *
		 */
		virtual bool handleMouseScroll(MouseEventArgs e);
		
		/**
		 * Handle mouse button up/down events from the window. The entity is 
		 * trusted to only handle mouse events if they are relevant to itself.
		 *
		 * If the entity has handled the event, it should return true. The 
		 * parent entity will then stop looking through it's children to 
		 * see who will handle the event.
		 * 
		 * @param e		Mouse event data
		 * @return		true if the Entity has handled the event
		 *
		 */
		virtual bool handleMouseButton(MouseEventArgs e);
		
		/**
		 * Handle the mouse entering the entity.
		 *
		 * @param e		Mouse event data
		 *
		 */
		virtual void handleMouseEnter(MouseEventArgs e);
		
		/**
		 * Handle the mouse leaving the entity.
		 *
		 * @param e		Mouse event data
		 *
		 */
		virtual void handleMouseLeave(MouseEventArgs e);
		
		/**
		 * Handle keyboard events from the window. The entity is 
		 * trusted to only handle keyboard events if they are relevant to itself.
		 *
		 * If the entity has handled the event, it should return true. The 
		 * parent entity will then stop looking through it's children to 
		 * see who will handle the event.
		 * 
		 * @param e		Keyboard event data
		 * @return		true if the Entity has handled the event
		 *
		 */
		virtual bool handleKey(KeyEventArgs e);
		
		/**
		 * Hit test for this Entity, given the mouse x,y coordinates. 
		 * The base implementation does a simple check to see if the mouse 
		 * is inside the bounds of the Entity (x, y, width, height). 
		 *
		 * This isn't actually used by any algorithm in Entity or Parsec. 
		 * Purely available for convenience. 
		 *
		 * @param x		X-coordinate of mouse
		 * @param y		Y-coordinate of mouse
		 *
		 */
		virtual bool hitTest(double x, double y);
		
		std::string	_name;
		
		Point 		_position;
		Size 		_size;
		Color		_background_color;
		Color		_foreground_color;
		
		bool 		_focused;
		bool		_entered;
		bool		_mouse_down;
		
		Entity * 				_parent;
		std::list<UIElement *>	_children;
		
		static int _counter;
	};

}

#endif

