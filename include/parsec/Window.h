/**
 * Window
 * ----------------------------------------------------------------------------
 * OS specific window
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#ifndef PARSEC_WINDOW_H_
#define PARSEC_WINDOW_H_

#include <string>
#include <parsec/Entity.h>
#include <parsec/Size.h>
#include <parsec/Point.h>
#include <parsec/UIElement.h>

namespace parsec
{
	class Window : public Entity
	{
	
	friend class Parsec;
	
	public:
	
		/**
		 * Constructor
		 * Creates a window with the specified title and size.
		 *
		 * @param title		Window title
		 * @param size		Window size in pixels
		 *
		 */
		Window(std::string title, Size size);
	
		/**
		 * Gets the title of the Window
		 *
		 */
		std::string title() const
		{
			return _title;
		}
		
		/**
		 * Sets the title for the Window
		 *
		 * @param title		New title for the window
		 *
		 */
		void setTitle(std::string title)
		{
			_title = title;
		}
		
		/**
		 * Sets the size of the window in pixels.
		 *
		 * @param width		Width of the window
		 * @param height	Height of the window
		 */
		virtual void setSize(int width, int height)
		{
			Entity::setSize(width, height);
		}
		
		/**
		 * Sets the position of the window in pixels relative from the top left
		 * corner of the screen.
		 *
		 * @param x			X coordinate in pixels relative to top-left of screen
		 * @param y			Y coordinate in pixels relative to top-left of screen
		 */
		virtual void setPosition(int x, int y)
		{
			Entity::setPosition(x, y);
		}
		
		/**
		 * Render the window contents
		 *
		 */
		virtual void render();
		
		/**
		 * Handle any events in the window.
		 *
		 */
		virtual void think();
		
		/**
		 * Has this window been closed? If this method returns true, the Window in 
		 * question will be NULL VERY VERY soon. This is mostly for use by Parsec.
		 * But feel free to play games for your own pleasure if that's your thing.
		 *
		 */
		bool closed() const
		{
			return _closed;
		}
	
	protected:
	
		std::string 		_title;
		sf::RenderWindow  	_window;
		bool				_closed;
	};
}

#endif
