/**
 * Bounds
 * ----------------------------------------------------------------------------
 * 2D bounds (position + size)
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#ifndef PARSEC_BOUNDS_H_
#define PARSEC_BOUNDS_H_

#include <parsec/Point.h>
#include <parsec/Size.h>

namespace parsec
{
	struct Bounds
	{
		Bounds(Point p, Size s)
		{
			position = p;
			size = s;
		}
		
		Bounds(int x, int y, int width, int height)
		{
			position.x = x;
			position.y = y;
			size.width = width;
			size.height = height;
		}
	
		Point position;
		Size  size;		
	};
}

#endif
