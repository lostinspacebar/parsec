/**
 * UI Element
 * ----------------------------------------------------------------------------
 * Base UI element. This has no visible representation but contains some base 
 * functionality for all UI elements.
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#ifndef PARSEC_UIELEMENT_H_
#define PARSEC_UIELEMENT_H_

#include <string>
#include <parsec/Entity.h>

namespace parsec
{
	class UIElement : public Entity
	{
	friend class Window;
	
	public:
	
		UIElement() : Entity()
		{
		}
	
		UIElement(std::string name) : Entity(name)
		{
		}
		
		std::string text() const
		{
			return _text;
		}
		
		void setText(std::string text)
		{
			_text = text;
		}
		
	protected:
	
		std::string _text;
		
	};
}

#endif
