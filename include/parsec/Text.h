/**
 * Text
 * ----------------------------------------------------------------------------
 * Simple text layer. Draws text on screen. Doesn't care about events.
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#ifndef PARSEC_TEXT_H_
#define PARSEC_TEXT_H_

#include <string>
#include <GLFW/glfw3.h>
#include <parsec/UIElement.h>

namespace parsec
{
	class Text : public UIElement
	{
		
	public:
	
		Text();
		
		/** 
		 * Renders the text
		 *
		 */
		virtual void render();
		
	protected:
	
		/**
		 * Text doesn't respond to any events.
		 *
		 */
		virtual bool hitTest(double x, double y)
		{
			return false;
		}
		
		GLuint _texture_id;
		
	};
}

#endif
