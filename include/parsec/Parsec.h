/**
 * Parsec Library Header
 * ----------------------------------------------------------------------------
 * Main library / API header.
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */

#ifndef PARSEC_H_
#define PARSEC_H_

#include <set>
#include <SFML/Window.hpp>
#include <parsec/Window.h>

namespace parsec
{

	class Parsec
	{
		friend class Window;

	public:

		/**
		 * Initializes the Parsec library and the underlying OpenGL ... stuff.
		 *
		 */
		static void initialize();
		
		/**
		 * Gets the time elapsed since  the application has started - like 
		 * the uptime of the application.
		 *
		 * @return sf::Time	data for time elapsed
		 */
		static sf::Time time();
		
		/**
		 * Perform one think iteration for Parsec. This allows the application to 
		 * perform any non-drawing logic before render() is called.
		 *
		 */
		static void think();
		
		/**
		 * Render contents of all windows.
		 *
		 */
		static void render();
		
		/**
		 * Shutdown Parsec including any windows that are registered.
		 *
		 */
		static void shutdown();
		
		/**
		 * Gets the current FPS of the Parsec system.
		 *
		 */
		static int fps()
		{
			return _fps;
		}
		
	private:
	
		/**
		 * Register a window to be rendered in the main application loop.
		 *
		 * @param window		Window to render in the main application loop.
		 *
		 */
		static void registerWindow(Window *window);
		
		/**
		 * De-register a window from being rendered in the main application loop.
		 *
		 * @param window		Window to render in the main application loop.
		 *
		 */
		static void deregisterWindow(Window *window);
		
		// System clock maintained by SFML
		static sf::Clock	_systemClock;
		
		// Registered windows that need to have their contents rendered.
		static std::set<Window *> _registered_windows;
		
		// FPS state
		static int 		_fps;
		static int 		_fps_counter;
		static float 	_fps_last_time;
	
	};

}

#endif
