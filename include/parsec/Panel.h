/**
 * UI Panel
 * ----------------------------------------------------------------------------
 * Simple UI panel. Usually used as a container for other UIElements.
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#ifndef PARSEC_PANEL_H_
#define PARSEC_PANEL_H_

#include <parsec/UIElement.h>

namespace parsec
{
	class Panel : public UIElement
	{
	public:
	
		Panel() : UIElement()
		{
		}
		
		Panel(std::string name) : UIElement(name)
		{
		}
		
		virtual void think() { };
	
		/** 
		 * Renders the panel and all its children.
		 *
		 */
		virtual void render(Bounds bounds);
		
	private:
	
		sf::RectangleShape _shape;
	
	};
}

#endif
