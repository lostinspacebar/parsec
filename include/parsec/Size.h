/**
 * Size
 * ----------------------------------------------------------------------------
 * 2D integer Size structure
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#ifndef PARSEC_SIZE_H_
#define PARSEC_SIZE_H_

namespace parsec
{
	struct Size
	{	
		Size()
		{
			width = 0;
			height = 0;
		}
	
		Size(int w, int h)
		{
			width = w;
			height = h;
		}
		
		int width;
		int height;
		
	};
}

#endif
