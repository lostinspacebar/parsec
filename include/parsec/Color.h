/**
 * Color
 * ----------------------------------------------------------------------------
 * Color data
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#ifndef PARSEC_COLOR_H_
#define PARSEC_COLOR_H_

namespace parsec
{
	struct Color
	{	
		Color()
		{
			r = 0.0f;
			g = 0.0f;
			b = 0.0f;
			a = 0.0f;
		}
		
		Color(float red, float green, float blue, float alpha = 1.0f)
		{
			r = red;
			g = green;
			b = blue;
			a = alpha;
		}
	
		float r;
		float g;
		float b;
		float a;
		
	};
}

#endif
