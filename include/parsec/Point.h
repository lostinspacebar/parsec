/**
 * Point
 * ----------------------------------------------------------------------------
 * 2D integer Point structure
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#ifndef PARSEC_POINT_H_
#define PARSEC_POINT_H_

namespace parsec
{
	struct Point
	{	
		Point()
		{
			x = 0;
			y = 0;
		}
	
		Point(int _x, int _y)
		{
			x = _x;
			y = _y;
		}
		
		int x;
		int y;
		
	};
}

#endif
