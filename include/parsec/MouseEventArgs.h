/**
 * Mouse Event Data
 * ----------------------------------------------------------------------------
 * Data related to a mouse event
 *
 * ----------------------------------------------------------------------------
 * Parsec GUI Library
 * Copyright 2014 Aditya Gaddam
 * www.lostinspacebar.com/projects/parsec
 */
 
#ifndef PARSEC_MOUSEEVENTARGS_H_
#define PARSEC_MOUSEEVENTARGS_H_

#include <parsec/KeyEventArgs.h>

namespace parsec
{
	/**
	 * Common mouse button definitions
	 *
	 */
	enum MouseButton
	{
		MOUSE_LEFT_BUTTON = 1,
		MOUSE_RIGHT_BUTTON = 2,
		MOUSE_MIDDLE_BUTTON = 3
	};
	
	/**
	 * Mouse event data
	 *
	 */
	struct MouseEventArgs
	{
		MouseEventArgs()
		{
			x = y = x_scroll = y_scroll = 0.0;
			handled = false;
		}
	
		double 	x;
		double 	y;
		double	x_scroll;
		double	y_scroll;
		int		button;
		int 	action;
		int		modifiers;
		bool	handled;
	};	
	
}

#endif
